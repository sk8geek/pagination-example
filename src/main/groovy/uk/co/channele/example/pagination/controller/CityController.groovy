package uk.co.channele.example.pagination.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView
import uk.co.channele.example.pagination.service.CityService

@Controller("/")
class CityController {

    @Autowired
    private CityService service

    @GetMapping("/")
    ModelAndView index(@RequestParam(name = "max", defaultValue = "2") Integer max, @RequestParam(name = "offset", defaultValue = "0") Integer offset) {
        def modelAndView = new ModelAndView("index")
        Page page = service.list(PageRequest.of(offset, max))
        modelAndView.addObject("page", page)
        return modelAndView
    }

}
