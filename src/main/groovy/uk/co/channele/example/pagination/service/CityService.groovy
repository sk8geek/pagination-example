package uk.co.channele.example.pagination.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import uk.co.channele.example.pagination.model.City
import uk.co.channele.example.pagination.model.Country
import uk.co.channele.example.pagination.repo.CityRepo


@Service
class CityService {

    static final List<Country> COUNTRIES = new ArrayList<>()
    static final List<City> CITIES = new ArrayList<>()

    static {

        COUNTRIES.add(new Country("Britain"))
        COUNTRIES.add(new Country("France"))
        COUNTRIES.add(new Country("Germany"))

        CITIES.add(new City("London", COUNTRIES.find { it.countryName == "Britain" }))
        CITIES.add(new City("Leeds", COUNTRIES.find { it.countryName == "Britain" }))
        CITIES.add(new City("Paris", COUNTRIES.find { it.countryName == "France" }))
        CITIES.add(new City("Bordeaux", COUNTRIES.find { it.countryName == "France" }))
        CITIES.add(new City("Berlin", COUNTRIES.find { it.countryName == "Germany" }))
        CITIES.add(new City("Hamburg", COUNTRIES.find { it.countryName == "Germany" }))

    }

    @Autowired
    private CityRepo repo


    Page<City> list(Pageable pageable) {
        return repo.findAll(pageable)
    }

}
