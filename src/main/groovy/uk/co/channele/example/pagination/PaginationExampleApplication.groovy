package uk.co.channele.example.pagination

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class PaginationExampleApplication {

	static void main(String[] args) {
		SpringApplication.run(PaginationExampleApplication, args)
	}

}
