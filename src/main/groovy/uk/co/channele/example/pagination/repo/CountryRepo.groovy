package uk.co.channele.example.pagination.repo

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import uk.co.channele.example.pagination.model.Country

@Repository
interface CountryRepo extends JpaRepository<Country, Long> {

}
