package uk.co.channele.example.pagination.model

import javax.annotation.Generated
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne

@Entity
class City {

    City() {}

    City(String name, Country country) {
        this.name = name
        this.country = country
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id

    @Column
    String name

    @OneToOne
    @JoinColumn
    Country country

}
