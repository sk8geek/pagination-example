package uk.co.channele.example.pagination.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import uk.co.channele.example.pagination.repo.CityRepo
import uk.co.channele.example.pagination.repo.CountryRepo


@Component
class SetupService implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private CityRepo cityRepo

    @Autowired
    private CountryRepo countryRepo

    @Override
    void onApplicationEvent(ApplicationReadyEvent event) {
        countryRepo.saveAll(CityService.COUNTRIES)
        cityRepo.saveAll(CityService.CITIES)
    }

}
