package uk.co.channele.example.pagination.repo

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import uk.co.channele.example.pagination.model.City

@Repository("City")
interface CityRepo extends JpaRepository<City, Long> {

    Page<City> findAll(Pageable pageable)

}