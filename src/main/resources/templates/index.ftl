<#import "utils.ftl" as utils/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cities</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
          crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="row">

        <h1>Cities</h1>

        <table class="table">
            <thead>
            <tr>
                <th>City</th>
                <th>Country</th>
            </tr>
            </thead>
            <tbody>
            <#list page.content as city>
                <tr>
                    <td>${city.name}</td>
                    <td>${city.country.countryName}</td>
                </tr>
            </#list>
            </tbody>
        </table>

        <@utils.pagination page=page/>

    </div>
</div>
</body>
</html>