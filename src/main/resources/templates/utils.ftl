<#macro pagination page>
    <#import "/spring.ftl" as spring />
    <#if (page.totalPages > 1)>
        <nav role="navigation" aria-label="Pagination Navigation">
            <ul class="pagination">
                <#if (page.pageable.pageNumber > 0)>
                    <li class="page-item">
                        <a class="page-link" aria-label="Goto previous page" href="<@spring.url '${springMacroRequestContext.requestUri}?offset=${page.pageable.pageNumber - 1}&max=${page.pageable.pageSize}'/>">Prev</a>
                    </li>
                </#if>
                <#list 1..page.totalPages as p>
                    <li class="page-item <#if page.pageable.pageNumber == p_index>active disabled</#if>">
                        <a class="page-link" aria-label="${(page.pageable.pageNumber== p_index)?then("Current page, page ${page.pageable.pageNumber + 1}","Goto page ${p_index + 1}")}"
                           <#if (page.pageable.pageNumber== p_index)>aria-current="true"</#if>
                           href="<@spring.url '${springMacroRequestContext.requestUri}?offset=${p_index}&max=${page.pageable.pageSize}'/>">${p}</a>
                    </li>
                </#list>
                <#if (page.pageable.pageNumber < page.totalPages - 1)>
                    <li class="page-item">
                        <a class="page-link" aria-label="Goto next page" href="<@spring.url '${springMacroRequestContext.requestUri}?offset=${page.pageable.pageNumber + 1}&max=${page.pageable.pageSize}'/>">Next</a>
                    </li>
                </#if>
            </ul>
        </nav>
    </#if>
</#macro>